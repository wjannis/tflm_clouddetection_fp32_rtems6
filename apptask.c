#include "system.h"
#include "inttypes.h"

#include <stdio.h>
#include <stdlib.h>

#include "main_functions.h"

rtems_task Application_task(
  rtems_task_argument argument
)
{
  rtems_id          tid;
  rtems_status_code status;
  unsigned int      a = (unsigned int) argument;
  status = rtems_task_ident( RTEMS_WHO_AM_I, RTEMS_SEARCH_ALL_NODES, &tid );
  //directive_failed( status, "ident" );

  printf(
    "Application task was invoked with argument (%d) "
    "and has id of 0x0%" PRIx32 " \n", a, tid
  );

  int i = 0;
  setup();
  while (i < 20) {
    loop();
    i++;
  }

  //TEST_END();
  exit( 0 );
}
